import {combineReducers} from 'redux';
import {reducer as userReducer} from './user/reducers';
import error from './error';
import loading from './loading';


const reducer = combineReducers({
  user: userReducer
});

export {reducer};
