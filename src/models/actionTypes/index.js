export const LOADING = 'LOADING';

export const SET_FCM_DEVICE_TOKEN = 'SET_FCM_DEVICE_TOKEN';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const SET_USER = 'SET_USER';

export const GET_QUESTIONS = 'GET_QUESTIONS';
export const SAVE_QUESTION = 'SAVE_QUESTION';

export const IS_LOADING = 'IS_LOADING';